apt update && apt install software-properties-common wget jq git gnupg unzip gnupg1 gnupg2 openssh-server python python3-apt curl ca-certificates perl iproute2 vim -y

sed -ri 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
sed -ri 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
sed -ri 's/^UsePAM yes/UsePAM no/' /etc/ssh/sshd_config

passwd -d root

mkdir /run/sshd

# EXPOSE 22
