# Objet : Fichier terraform création EC2 pour le projet NSA
# Instances : SRV-Back-NSA , SRV-Front-NSA , SRV-DB-NSA , SRV-GITLAB-NSA
# Région : eu-west-3
# Auteur :
# Date : 21/01/2022

variable "SUBNET_ID" {
  type = string
}

variable "VPC_SECURITY_GROUP_IDS" {
    type = string
}

variable "SSH_PUB_KEY" {
    type = string
}

provider "aws" {
	profile    = "default"
	region = "eu-west-3"
}

resource "aws_key_pair" "admin" {
    key_name   = "admin"
    public_key = var.SSH_PUB_KEY
}

resource "aws_security_group" "ingress-ssh-test" {
  name   = "allow-ssh-sg"
  vpc_id = "vpc-059e834f5bb6a95d1"

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]

    from_port = 22
    to_port   = 22
    protocol  = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ingress-http-test" {
  name   = "allow-http-sg"
  vpc_id = "vpc-059e834f5bb6a95d1"

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]

    from_port = 80
    to_port   = 80
    protocol  = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ingress-https-test" {
  name   = "allow-https-sg"
  vpc_id = "vpc-059e834f5bb6a95d1"

  ingress {
    cidr_blocks = [
      "0.0.0.0/0"
    ]

    from_port = 443
    to_port   = 443
    protocol  = "tcp"
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_eip_association" "eip_assoc" {
    instance_id   = aws_instance.SRV-Back-NSA.id
    allocation_id = "eipalloc-0370fb5310705ec6c"
}

resource "aws_instance" "SRV-Back-NSA" {
		# AMI >> Debian 10 (HVM), SSD Volume Type
		ami						= "ami-04e905a52ec8010b2"
		instance_type			= "t2.micro"

		key_name				= "admin"

		subnet_id				= var.SUBNET_ID
		vpc_security_group_ids  = [var.VPC_SECURITY_GROUP_IDS]

		root_block_device{
		  volume_size			= "80"
		  volume_type			= "gp3"
		  }
    # provisioner "file" {
    # source="script.sh"
    # destination="/tmp/script.sh"
    # }
    user_data = <<-EOF
        #!/bin/bash

        apt update && apt install software-properties-common wget jq git gnupg unzip gnupg1 gnupg2 openssh-server python python3-apt curl ca-certificates perl iproute2 vim -y
        sudo apt -y install lsb-release apt-transport-https ca-certificates
        sudo wget -O /etc/apt/trusted.gpg.d/php.gpg https://packages.sury.org/php/apt.gpg
        sudo echo "deb https://packages.sury.org/php/ $(lsb_release -sc) main" | sudo tee /etc/apt/sources.list.d/php.list
        sudo apt update
        sed -ri 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config
        sed -ri 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config
        sed -ri 's/^UsePAM yes/UsePAM no/' /etc/ssh/sshd_config
        passwd -d root
        mkdir /run/sshd
        sudo fallocate -l 2G /swapfile && sudo chmod 600 /swapfile && sudo mkswap /swapfile && sudo swapon /swapfile && sudo echo '/swapfile none swap defaults 0 0' >> /etc/fstab
        EOF
    #   user_data = file("script.sh")
    # provisioner "local-exec" {
    #     interpreter = ["/bin/bash" ,"-c"]
    #     command = <<-EOT
    #         exec "apt update && apt install software-properties-common wget jq git gnupg unzip gnupg1 gnupg2 openssh-server python python3-apt curl ca-certificates perl iproute2 vim -y"
    #         exec "sed -ri 's/#PermitEmptyPasswords no/PermitEmptyPasswords yes/' /etc/ssh/sshd_config"
    #         exec "sed -ri 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config"
    #         exec "sed -ri 's/^UsePAM yes/UsePAM no/' /etc/ssh/sshd_config"
    #         exec "passwd -d root"
    #         exec "mkdir /run/sshd"

    #     EOT
    # }
#   provisioner "local-exec" {
#     command = "apt update && apt install software-properties-common wget jq git gnupg unzip gnupg1 gnupg2 openssh-server python python3-apt curl ca-certificates perl iproute2 vim -y"
#   }
	  tags = {
		Name			= "SRV-Back-NSA"
		"Application"	= "Web"
		"Environment"	= "PRD"
		"OS"			= "Debian"
		"Role"			= "Backend"
		}
}
